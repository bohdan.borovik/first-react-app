import { useState, useEffect, useContext } from "react";
import Product from "./Product";
import {Container, Row} from "react-bootstrap";
import Cart from "./Cart";
import ProfileContext from "../context/ProfileContext";

function Products() {
    const [products, setProducts] = useState([]);
    const {setMessage} = useContext(ProfileContext);

    useEffect(() => {
        setProducts([
            {id: 1,
            brand: 'Lenovo', 
            model: 'IdeaPad 3 15 Business Black', 
            imgSrc: 'https://hotline.ua/img/tx/340/340907978_s265.jpg', 
            price: 11855, 
            description: 'Ноутбук, класичний, 15,6", TN + Film, 1366x768, Intel Celeron N4020, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },
            {id: 2,
            brand: 'Lenovo', 
            model: 'V15 IGL', 
            imgSrc: 'https://hotline.ua/img/tx/340/340477564_s265.jpg', 
            price: 14541, 
            description: 'Ноутбук, Класичний, 15,6", 1920x1080, Intel Celeron N4020, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 3,
            brand: 'HP', 
            model: '255 G8', 
            imgSrc: 'https://hotline.ua/img/tx/341/341433360_s265.jpg', 
            price: 15797, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1366x768, AMD Athlon Silver 3050U, 2,3 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 4,
            brand: 'ASUS', 
            model: 'X515MA Transparent Silver', 
            imgSrc: 'https://hotline.ua/img/tx/341/341376199_s265.jpg', 
            price: 15559, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1920x1080, Intel Celeron N4020, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 5,
            brand: 'YEPO', 
            model: '737i5 8/512', 
            imgSrc: 'https://hotline.ua/img/tx/340/340246489_s265.jpg', 
            price: 15900, 
            description: 'Ноутбук, Класичний, 15,6", IPS, 1920x1080, Intel Core i5-5200U, 2,2 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 6,
            brand: 'Acer', 
            model: 'Aspire 3 A315-34-P5KW Charcoal Black', 
            imgSrc: 'https://hotline.ua/img/tx/341/341142140_s265.jpg', 
            price: 16495, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1920x1080, Intel Pentium N5030, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 7,
            brand: 'Lenovo', 
            model: 'IdeaPad 3 15IGL05', 
            imgSrc: 'https://hotline.ua/img/tx/341/341081332_s265.jpg', 
            price: 16499, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1920x1080, Intel Pentium N5030, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 8,
            brand: 'HP', 
            model: '255 G8', 
            imgSrc: 'https://hotline.ua/img/tx/341/341433357_s265.jpg', 
            price: 16667, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1366x768, AMD Ryzen 3 3250U, 2,6 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 9,
            brand: 'Acer', 
            model: 'Aspire 3 A315-23 Black', 
            imgSrc: 'https://hotline.ua/img/tx/339/339061678_s265.jpg', 
            price: 17144, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1920x1080, AMD 3020e, 1,2 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 10,
            brand: 'HP', 
            model: '15-ef1001ds', 
            imgSrc: 'https://hotline.ua/img/tx/338/338948847_s265.jpg', 
            price: 17686, 
            description: 'Ноутбук, Класичний, 15,6", 1366x768, AMD Athlon Gold 3150U, 2,4 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 11,
            brand: 'Lenovo', 
            model: 'IdeaPad 3 15IIL05', 
            imgSrc: 'https://hotline.ua/img/tx/329/329634901_s265.jpg', 
            price: 17920, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1920x1080, Intel Core i3-1005G1, 1,2 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 12,
            brand: 'Acer', 
            model: 'Aspire 3 A315-34-P3AC', 
            imgSrc: 'https://hotline.ua/img/tx/334/334669068_s265.jpg', 
            price: 17999, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1920x1080, Intel Pentium Silver N5030, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 13,
            brand: 'HP', 
            model: '250 G8', 
            imgSrc: 'https://hotline.ua/img/tx/341/341106279_s265.jpg', 
            price: 18000, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1366x768, Intel Core i3-1005G1, 1,2 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 14,
            brand: 'CHUWI', 
            model: 'GemiBook X', 
            imgSrc: 'https://hotline.ua/img/tx/340/340773164_s265.jpg', 
            price: 18483, 
            description: 'Ноутбук, Класичний, 15,6", IPS, 1920x1080, Intel Celeron N5100, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            },{id: 15,
            brand: 'ASUS', 
            model: 'X515JA', 
            imgSrc: 'https://hotline.ua/img/tx/341/341397747_s265.jpg', 
            price: 18566, 
            description: 'Ноутбук, Класичний, 15,6", TN + film, 1920x1080, Intel Pentium Gold 6805, 1,1 ГГц, ОЗП: 8 ГБ',
            addedToCart: false,
            count: 0
            }
        ])
    }, [])

    const addToCart = id => {
        setProducts(products.map(product => ({...product,
            addedToCart: product.id === id ? true : product.addedToCart, 
            count: product.id === id ? 1 : product.count}))
        );
        setMessage(`Додано до кошика`);
    }

    const removeFromCart = id => {
        setProducts(products.map(product => ({...product,
            addedToCart: product.id === id ? false : product.addedToCart, 
            count: product.id === id ? 0 : product.count}))
        );
        setMessage(`Видалено з кошика`);
    }

    const addItem = id => {
        setProducts(products.map(product => ({...product,
            count: product.id === id ? product.count + 1 : product.count}))
        );
    }

    const removeItem = id => {
        setProducts(products.map(product => ({...product,
            count: product.id === id ? product.count - 1 : product.count}))
        );
    }

    return <Container className="my-3">
        <Row>
            {products.map(product => <Product key={product.id} 
                                              product={product} 
                                              addToCart={addToCart}
                                              removeFromCart={removeFromCart}/>)}
        </Row>
        {products.filter(product => product.addedToCart).length ? <Cart products={products.filter(product => product.addedToCart)} removeFromCart={removeFromCart} addItem={addItem} removeItem={removeItem}/> : ''}
    </Container>
}
export default Products;