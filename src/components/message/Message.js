import Alert from 'react-bootstrap/Alert';

import './message.css'

function Message({message}) {
    return <div className='info-message'>
        <Alert show={message ? true : false} variant="primary">
            <Alert.Heading>{message}</Alert.Heading>
        </Alert></div>
}

export default Message;
