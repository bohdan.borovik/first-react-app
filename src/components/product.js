import {Card, Button, Col} from "react-bootstrap";

function Product({product, addToCart, removeFromCart}) {
    return <Col sm={6} md={4} lg={3}>
    <Card className="my-2">
        <Card.Img variant="top" src={product.imgSrc} />
        <Card.Body>
            <Card.Title>{product.brand}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{product.model}</Card.Subtitle>
            <Card.Text>{product.description}</Card.Text>
            <Card.Text className="text-right"><h6>{product.price.toFixed(2)} грн</h6></Card.Text>
            <div className="text-center">
                {product.addedToCart ? 
                    <Button variant="danger" className="mx-1" onClick={() => removeFromCart(product.id)}>Видалити</Button>: 
                    <Button variant="primary" className="mx-1" onClick={() => addToCart(product.id)}>Придбати</Button>}
            </div>
        </Card.Body>
    </Card>
    </Col>
}
export default Product;