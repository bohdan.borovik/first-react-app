import './Cart.css';
import { ListGroup } from 'react-bootstrap';
import { useState, useEffect} from "react";
import CartItem from './Cartitem';

function Cart({products, removeFromCart, addItem, removeItem}) {
    
    const [total, setTotal] = useState(0);

    useEffect(() =>
        setTotal(products.reduce((acc, product) => acc + product.price*product.count, 0))
    , [products])

    return <div className="cart-box">
        <h3>Кошик</h3>
        <ListGroup>
            {products.map(product => 
            <CartItem product={product} removeFromCart={removeFromCart} addItem={addItem} removeItem={removeItem} />
            )}
        </ListGroup>
        <p className='price-label'>Всього - {total.toFixed(2)} грн</p>
    </div>
}

export default Cart;