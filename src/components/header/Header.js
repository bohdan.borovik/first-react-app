import EnterProfile from "./EnterProfile";
import Profile from "./Profile";

import ProfileContext from "../../context/ProfileContext";
import { useContext } from "react";

import './Header.css'

function Header(){
    const {user} = useContext(ProfileContext);
    return <header className="bg-primary head-height">
        <div className="container">
            {user.login ? <Profile /> : <EnterProfile />}
        </div>
    </header>
}

export default Header;