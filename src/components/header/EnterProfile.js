import ProfileContext from "../../context/ProfileContext";
import {useRef, useContext, useEffect} from "react";
import { Button, Form, Row, FormControl } from "react-bootstrap";

function EnterProfile() {
    const loginRef = useRef(null);
    const passwordRef = useRef(null);

    const {setUser, setMessage} = useContext(ProfileContext);

    useEffect(() => {
        let local = localStorage.getItem('...');    
        let localUser = JSON.parse(local);
        localUser ? SetDataUser(localUser.login, localUser.password) : SetDataUser('', '');
    }, []);

    function SetUserMessage(login) {
        setMessage(`Вітаю, ${login}`)
    }

    function SetDataUser(log, pass) {
        setUser({login: log, password: pass});
    }

    function message(){
        passwordRef.current.value = '';
        alert( "Введіть пароль довше 5-ти символів" );
    }
    
    function submitProfile() {
        let log = loginRef.current.value;
        let pass = passwordRef.current.value;
        let passLength = pass.length;
        (passLength >= 5) ? setUser({login: log, password: pass}) : message();
        localStorage.setItem('...',JSON.stringify({login: log, password: passLength}));
        SetUserMessage(log);
    }

    return<Row className="mx-1 justify-content-end">
        <Form className="d-flex my-2">
            <FormControl type="text"
                            ref={loginRef}
                            placeholder="Login"></FormControl>
            <FormControl className="mx-2" type="password"
                            ref={passwordRef}
                            placeholder="Password"></FormControl>
            <Button variant="danger" onClick={submitProfile}>Увійти</Button>
        </Form>
    </Row>
}

export default EnterProfile;