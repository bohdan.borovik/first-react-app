import { useContext } from "react";
import { Row } from "react-bootstrap";
import ProfileContext from "../../context/ProfileContext";

function Profile() {
    const {user} = useContext(ProfileContext);

    return <Row className="mx-1 justify-content-end user-center">
        <div>Вітаю, {user.login}</div>
    </Row>
}

export default Profile;