import Badge from 'react-bootstrap/Badge';
import { ListGroup } from 'react-bootstrap';
import Counter from './Counter';

function CartItem({product, removeFromCart, addItem, removeItem}) {
    return <ListGroup.Item key={product.id}>
        {product.brand} {product.model} - {product.price.toFixed(2)} грн
        <Badge bg="danger" className='ml-4 pointer' text="white" onClick={() => removeFromCart(product.id)}>Видалити</Badge>
        <p>
            <Counter product={product} 
                        removeItem={removeItem} 
                        addItem={addItem} />
        </p>
        </ListGroup.Item>
}

export default CartItem;