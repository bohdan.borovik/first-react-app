import Badge from 'react-bootstrap/Badge';
import { useState } from "react";


function Counter({product, removeItem, addItem}) {

    const name = product.count <= 1 ? 'mr-2 badge-disabled' : 'mr-2 pointer';

    return <>
        <Badge bg="secondary" className={name} text="white" onClick={() => removeItem(product.id)}>-</Badge>
            <span>{product.count}</span>
        <Badge bg="secondary" className='ml-2 pointer' text="white" onClick={() => addItem(product.id)}>+</Badge>
    </>
    
}

export default Counter;