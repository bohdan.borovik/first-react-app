// import './App.css';
import {useState} from "react";

function TestButton() {
    // Объявление переменной состояния, которую мы назовём "count"
    const [count, setCount] = useState(0);
    var [stroka, setStroka] = useState('раз');

    function add() {
      setCount(count + 1);
    };
    function remove() {
      setCount(count - 1);
    };
    function check() {
        let str = count.toString();
        if(str.includes('12') || str.includes('13') || str.includes('14')) {
          setStroka(stroka = `раз`)
      } else if(str.includes('2') || str.includes('3') || str.includes('4')) {
            setStroka(stroka = `раза`)
        } else {
            setStroka(stroka = `раз`)
            // debugger
        }
    }
  
    return (
      <div>
        <p>Вы кликнули {count} {stroka}</p>
        <button onClick={check} onMouseDown={remove}>Remove</button>
        <button onClick={check} onMouseDown={add}>Add</button>
      </div>
    );
  }

export default TestButton;  