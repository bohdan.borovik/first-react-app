import { useState } from 'react';
import Header from './components/header/Header';
import Message from './components/message/Message';
import Products from './components/Products';
import ProfileContext from './context/ProfileContext';

function App() {
  const [user, setUser] = useState({login:'', password:''});
  const [message, setMessage] = useState('');

  return (
    <div className="bg-dark">
      <ProfileContext.Provider value={{user, setUser, setMessage}}>
        <Header />
        <Products />
        <Message message={message}/>
      </ProfileContext.Provider>
    </div>
  );
}

export default App;